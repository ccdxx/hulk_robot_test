#!/bin/bash

function import_openeuler_jenkins() {
    rm -rf ${shell_path}
    for i in $(seq 300);do
        git clone --depth 1 https://gitee.com/openeuler/openeuler-jenkins.git ${shell_path} 1> /dev/null
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    if [[ $exitcode -ne 0 ]];then
        log_error "git clone openeuler-jenkins failed"
    fi
    . ${shell_path}/src/lib/lib.sh
    owner="openeuler"
    jenkins_api_host="https://openeulerjenkins.osinfra.cn/"
    export PYTHONPATH=$shell_path
}

function install_build_tools() {
    log_info "***** Start to install build tools *****"
    sudo yum install -y gcc-c++ flex bison elfutils-libelf-devel openssl-devel dwarves gmp-devel libmpc-devel make 1>/dev/null
    log_info "***** End to install build tools *****"
}

function download_openeuler_kernel() {
    log_info "***** Start to download kernel of nestos *****"
    rm -rf $current_path/openeuler/nestos-kernel
    mkdir -p $current_path/openeuler
    cd $current_path/openeuler
    for i in $(seq 300);do
        git clone -b $tbranch --depth 1 https://gitee.com/openeuler/nestos-kernel.git 1> /dev/null
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    if [[ $exitcode -ne 0 ]];then
        log_error "git clone nestos-kernel failed"
    fi
    cd $current_path/openeuler/nestos-kernel
    log_info "***** End to download kernel of nestos *****"
}

function git_config() {
    git config --global user.email "openeuler@huawei.com"
    git config --global user.name "OpenEuler"
    git config --global http.postBuffer 1048576000
    git config --global core.compression -1
    git config --global http.lowSpeedLimit 0
    git config --global http.lowSpeedTime 3600
}

function git_am_pr() {
    log_info "***** Download and Apply PR *****"
    cd $current_path/openeuler/nestos-kernel
    wget https://gitee.com/openeuler/nestos-kernel/pulls/${prid}.patch 1> /dev/null
    git am ${prid}.patch
    if [[ $? -eq 0 ]]; then
        log_info "git am patch success"
    else
        log_error "git am patch failed"
    fi
}
