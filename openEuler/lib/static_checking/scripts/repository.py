import os
import subprocess

def execute_shell_unicode(args, shell=True, timeout=None, cwd=None):
    proc = subprocess.Popen(
        args,
        cwd=cwd,
        shell=shell,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
    )

    try:
        stdout, errs = proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        proc.kill()
        stdout, errs = shelllog.communicate()

    if not isinstance(stdout, str):
        stdout = stdout.decode()

    return proc.returncode, stdout

class GitRepository:

    @staticmethod
    def get_remote_branch(url, branch, path):
        cmds = "git ls-remote %s %s" % (url, branch)
        ret, msg = execute_shell_unicode(
            "git ls-remote %s %s" % (url, branch),
            cwd=path
        )
        for line in msg.split('\n'):
            attrs = line.split()
            if len(attrs) != 2:
                continue
            if attrs[1].find(branch) != -1:
                return attrs[0]
        return None

    @staticmethod
    def get_commit(path):
        ret, msg = execute_shell_unicode(
            "git log -n 1 --pretty=format:'%H|||%s'",
            cwd=path
        )
        for line in msg.split("\n"):
            attrs = line.split('|||')
            if len(attrs) != 2:
                continue
            return attrs[0]
        return None

    @staticmethod
    def get_subject(path, commit=''):
        ret, msg = execute_shell_unicode(
            "git log -n 1 --pretty=format:'%%s|||%%H' %s" % commit,
            cwd=path
        )
        for line in msg.split("\n"):
            attrs = line.split('|||')
            if len(attrs) != 2:
                continue
            return attrs[0]
        return None

    @staticmethod
    def get_config(path, config='remote.origin.url'):
        ret, msg = execute_shell_unicode(
            "git config %s" % config,
            cwd=path
        )
        for line in msg.split("\n"):
            if len(line) != 0:
                return line
        return None

    @staticmethod
    def is_branch_contains(path, branch, commit):
        ret, output = execute_shell_unicode(
            "git branch --contains %s | grep %s" % (commit, branch),
            cwd=path
        )
        if ret == 0 and len(output.strip()) != 0:
            return True
        else:
            return False

    @staticmethod
    def is_branch_merged(path, commit):
        ret, output = execute_shell_unicode(
            "git log --grep '%s'" % (commit),
            cwd=path
        )
        if ret == 0 and len(output.strip()) != 0:
            return True
        else:
            return False

    @staticmethod
    def get_commit_fixes(path, commit, subject):
        scommit = commit[:12]
    #    print("scommit")
    #    print(scommit)
        fixes = []
    #    print("subject")
    #    print(subject)
        ret, output = execute_shell_unicode(
            "git log --grep '%s' --grep \"\\\"%s\\\"\" --pretty=format:'%%H|||%%s'" % (
                scommit, subject
            ),
            cwd=path
        )
  #      print("ret:")
  #      print(ret)
  #      print("output:")
  #      print(output)
        for line in output.split("\n"):
            attrs = line.split('|||')
  #          print("attrs:")
   #         print(attrs)
            if len(attrs) != 2 or len(attrs[0]) != 40:
                continue
            if attrs[1].startswith('Merge '):
                continue
            if commit == attrs[0]:
                continue
            fixes.append({'commit': attrs[0], 'subject': attrs[1]})
  #      print("fixes:")
  #      print(fixes)
        return fixes
