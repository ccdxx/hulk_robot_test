#!/usr/bin/python3

import os
import re
import sys
import subprocess
import shlex
import getopt
import threading

dir = os.path.split(os.path.realpath(__file__))[0]
sys.path.append(dir)

from repository import GitRepository
from concurrent.futures import ThreadPoolExecutor

sys.path.append(dir + "/../")

from commons import linux_path
from commons import source_branch
from commons import target_branch

POOL_THREAD_NUM = 20


def execute_shell_unicode(args):
    if isinstance(args, str):
        shelllog = subprocess.Popen(args, shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    else:
        shelllog = subprocess.Popen(args, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)

    shellOut = shelllog.communicate()[0]

    if not isinstance(shellOut, str):
        shellOut = str(shellOut, errors='ignore')

    return shelllog.returncode, shellOut


def get_main_path():
    cmd = "pwd"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return False
    return out.strip()


main_path = get_main_path()


def update_linux():
    print("update linux kernel")
    cmd = "cd " + linux_path + " && git pull"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        print("fail to update linux")
        print(out)
        return False
    print("update linux success")
    return True


def do_checkdepend(patch):
    commit = patch.split(" ")[0]

    cmd = "git format-patch -1 " + commit + " -o checkdepend"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return False, out
    return True, out


def get_seriesfile(patches):
    for patch in patches:

        commit = patch.split(" ")[0]

        print(commit)

        cmd = "git format-patch -1 " + commit + " -o checkdepend"
        ret, out = execute_shell_unicode(cmd)

        if ret:
            return False, out

    cmd = "cat ./checkdepend/* > ./checkdepend/series.patch"
    ret, out = execute_shell_unicode(cmd)

    if ret:
        return False, out

    return True, 'checkdepend/series.patch'


def get_patchfile(patch):
    commit = patch.split(" ")[0]

    cmd = "git format-patch -1 " + commit + " -o checkdepend"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return False, out

    return True, out.strip()


def do_check_dependency(series, commit, subject, fixed, ignores, check_patch):
    commits = 1
    warnings = 0
    #    if GitRepository.is_branch_contains(linux_path, "master", commit):
    #        print("commit %s has been merged" % commit)
    #        return 2, 'total: 1 warnings, 1 commits checked'
    #
    #    if GitRepository.is_branch_merged(linux_path, commit):
    #        print("commit %s has been merged" % commit)
    #        return 2, 'total: 1 warnings, 1 commits checked'

    detail = ''
    miss_fixes = []
    for fix in GitRepository.get_commit_fixes(linux_path, commit, subject):
        commits = commits + 1
        if is_series_contains(fix['commit'], series):
            # print("fix %s is series contains" % fix['commit'])
            continue

        if GitRepository.is_branch_contains(main_path, source_branch, fix['commit']):
            # print("fix %s is branch contains" % fix['commit'])
            continue

        if GitRepository.is_branch_merged(main_path, fix['commit']):
            # print("fix %s is branch merged" % fix['commit'])
            continue

        miss_fixes.append(fix)
        warnings = warnings + 1
        detail += 'new bugfix %s ("%s") exists\n' % (
            fix['commit'][:12], fix['subject']
        )

    miss_fixed = []
    for fix in fixed:
        commits = commits + 1
        fcommit = fix['commit']
        subject = GitRepository.get_subject(linux_path, fcommit)
        if subject is None:
            # print("bad fixed commit %s" % fcommit)
            continue

        if is_series_contains(fix['commit'], series):
            # print("fixed %s is series contains" % fix['commit'])
            continue

        if GitRepository.is_branch_contains(main_path, source_branch, fix['commit']):
            # print("fixed %s is branch contains" % fix['commit'])
            continue

        if GitRepository.is_branch_merged(main_path, fix['commit']):
            # print("fixed %s is branch merged" % fix['commit'])
            continue

        miss_fixed.append(fix)
        warnings = warnings + 1
        detail += 'fixed patch %s ("%s") not merged\n' % (
            fix['commit'][:12], subject
        )

    if len(miss_fixes) == 0 and len(miss_fixed) == 0:
        return 1, 'total: 0 warnings, %s commits checked' % commits, check_patch

    return 2, 'total: %s warnings, %s commits checked\n\n%s' % (
        warnings, commits, detail
    ), check_patch


def is_series_contains(commit, series):
    if series:
        scommit = commit[:12]
        if series.find(commit) != -1:
            return True
        elif series.find("commit %s" % scommit) != -1:
            return True
        return False
    else:
        return False


def check_dependency(series, check_patch):
    ret, patch = get_patchfile(check_patch)
    if not ret:
        print(ret, patch)
        return False

    with open(patch, 'r') as fp:
        content = fp.read()

    _mainline = False
    _stable = False
    commit = None
    fixed = []
    ignores = []
    for line in content.split('\n'):
        line = line.strip()

        if re.search("^mainline inclusion$", line):
            _mainline = True
            # print("mainline inclusion")
            continue

        if re.search("^\\[ Upstream commit", line):
            _stable = True

        if re.search("^stable inclusion$", line):
            _stable = True
            # print("stable inclusion")
            continue

        if _mainline is False and _stable is False:
            continue

        if line.startswith("commit") or line.startswith("[ Upstream commit"):
            if _mainline and commit:
                continue

            if _stable and not any(['upstream' in line, line.startswith("[ Upstream commit")]):
                continue

            if re.search("[0-9a-f]{8,40}", line):
                commit = re.search("[0-9a-f]{8,40}", line).group(0)
            else:
                print("checkdepend: bad commit: %s" % line)
                commit = None

        elif line.startswith("Fixes:"):
            attrs = line.split()
            if len(attrs) < 2 or not re.search("[0-9a-f]{6,40}", attrs[1]):
                print("checkdepend: bad fixes: %s" % line)
                continue
            fixed.append({'commit': attrs[1], 'fixes': ' '.join(attrs[1:])})
        elif line.startswith("Ignores:"):
            attrs = line.split()
            if len(attrs) < 2 or not re.search("[0-9a-f]{6,40}", attrs[1]):
                print("checkdepend: bad ignores: %s" % line)
                continue
            ignores.append({'commit': attrs[1], 'ignores': ' '.join(attrs[1:])})

    if _mainline is False and _stable is False:
        print("skip not mainline/stable patch")
        return 1, 'total: 0 warnings, 0 commits checked', check_patch

    if commit is None:
        print("checkdepend: commit not found from patch")
        return 1, 'total: 0 warnings, 0 commits checked', check_patch

    subject = GitRepository.get_subject(linux_path, commit)
    if subject is None:
        print("commit %s not exists in mainline" % commit)
        return 2, 'total: 1 warnings, 1 commits checked', check_patch

    with open(os.path.join(series), 'r') as fp:
        series = fp.read().replace(content, '')

    return do_check_dependency(series, commit, subject, fixed, ignores, check_patch)


def clean():
    cmd = "rm -rf ./checkdepend"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return False, out


failed_result = []
lock = threading.Lock()


def get_result(thread_pool):
    lock.acquire()
    res, desp, patch = thread_pool.result()
    print("check: " + patch)
    if res != 1:
        failed_result.append([patch, desp])
    else:
        print("check success")
    lock.release()


def print_result():
    print("---- results ----")
    for res in failed_result:
        print("check failed: " + res[0])
        print(res[1])


def main(argv):
    if not linux_path:
        print("missing linux_path")
        return False

    single_patch = True
    if len(sys.argv) != 1:
        opts, args = getopt.getopt(argv[1:], 's:')
        patches = sys.argv[1:]
        for o, v in opts:
            if o == '-s':
                patches = os.popen("git log --oneline " + v + " -1").readlines()
                patches = patches + os.popen("git log --oneline " + v + "... | tac").readlines()
                single_patch = False
    else:
        tag = sys.argv[1]
        patches = os.popen("git log --oneline " + tag + "..HEAD").readlines()

    update_linux()
    clean()

    series_patch = patches
    if single_patch is True:
        cmd = "git log --oneline " + target_branch + "... | tac"
        series_patch = patches + os.popen(cmd).readlines()

    ret, out = get_seriesfile(series_patch)
    if ret == False:
        return False

    seriesfile = out

    pool = ThreadPoolExecutor(max_workers=POOL_THREAD_NUM)

    fail_des = ""
    failed_num = 0
    for patch in patches:
        patch = patch.strip()
        future = pool.submit(check_dependency, seriesfile, patch)
        future.add_done_callback(get_result)

    pool.shutdown()

    print_result()
    clean()

    return True


if __name__ == "__main__":
    # execute only if run as a script
    main(sys.argv)
