#!/usr/bin/python3

import os
import re
import sys
import subprocess
import shlex
import getopt
import json


committer_conf = {}


def execute_shell_unicode(args):
    if isinstance(args, str):
        shelllog = subprocess.Popen(args, shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    else:
        shelllog = subprocess.Popen(args, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    shellOut = shelllog.communicate()[0]
    if not isinstance(shellOut, str):
        shellOut = str(shellOut, errors='ignore')

    return shelllog.returncode, shellOut


def lookup_committer(path):
    global committer_conf
    committers = []
    committers.append(committer_conf["/"]["owner"])
    curr = committer_conf["/"]["children"]

    paths = path.split("/")
    for path in paths:
        if path not in curr:
            break
        else:
            owner = curr[path].get("owner", [])
            if len(owner) == 0:
                pass
            else:
                committers.append(owner)
            curr = curr[path].get("children", {})

    return committers


def get_committers(patch):
    commit = patch.split(" ")[0]

    cmd = "git diff " + commit + "~1.." + commit + " --name-only"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return 1, out

    committers = {}
    for path in out.splitlines():
        print("lookuping " + path)
        path = path.strip()
        committers[path] = lookup_committer(path)
    return 0, committers


def main(argv):
    if len(sys.argv) != 1:
        opts, args = getopt.getopt(argv[1:], 's:i:o:')
        patches = sys.argv[1:]
        for o, v in opts:
            if o == '-s':
                patches = os.popen("git log --oneline " + v + " -1").readlines()
                patches = patches + os.popen("git log --oneline " + v + "... | tac").readlines()
            elif o == '-i':
                conf_file = v
            elif o == '-o':
                output = v
    else:
        print("need parameters!")
        return -1

    global committer_conf
    with open(conf_file, 'r') as f:
        committer_conf = json.load(f)

    failed_num = 0
    patch_committers = {}
    for patch in patches:
        patch = patch.strip()
        print("checking " + patch)

        ret, description = get_committers(patch)
        if ret == 0:
            print("check " + patch + " success")
            patch_committers.update(description)
        else:
            print("check " + patch + " failed")
            print(description)
            failed_num = failed_num + 1

    if 'output' in locals():
        with open(output, 'w') as f:
            json.dump(patch_committers, f)

    print("---- result ----")
    print("total:" + str(len(patches)) + " failed:" + str(failed_num) +
          " success:" + str(len(patches) - failed_num))

    return failed_num


if __name__ == "__main__":
    # execute only if run as a script
    print('begin to get committer....')
    exit(main(sys.argv))
