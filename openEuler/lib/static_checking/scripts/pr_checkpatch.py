#!/usr/bin/python3

import os
import re
import sys
import subprocess
import shlex
import getopt


def execute_shell_unicode(args):
    if isinstance(args, str):
        shelllog = subprocess.Popen(args, shell=True, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    else:
        shelllog = subprocess.Popen(args, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT)
    shellOut = shelllog.communicate()[0]
    if not isinstance(shellOut, str):
        shellOut = str(shellOut, errors='ignore')

    return shelllog.returncode, shellOut


ignores_for_main = [
    'CONFIG_DESCRIPTION',
    'FILE_PATH_CHANGES',
    'GERRIT_CHANGE_ID',
    'GIT_COMMIT_ID',
    'UNKNOWN_COMMIT_ID',
    'FROM_SIGN_OFF_MISMATCH',
    'REPEATED_WORD',
    'COMMIT_COMMENT_SYMBOL',
    'BLOCK_COMMENT_STYLE',
    'AVOID_EXTERNS',
    'AVOID_BUG'
]


def clean():
    cmd = "rm -rf ./checkpatch"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return False, out


def is_from_mainline(patch):
    content = ''
    with open(patch, 'r') as fp:
        content = fp.read()
    for line in content.split('\n'):
        line = line.strip()
        if line in ["mainline inclusion"]:
            return True
        # stable backport
        if re.match("commit [0-9a-f]{8,40} upstream", line, re.IGNORECASE):
            return True
        # stable backport
        if re.match("\[\s*upstream commit [0-9a-f]{8,40}\s*\]", line, re.IGNORECASE):
            return True
        # stable backport
        if line in ["stable inclusion"]:
            return True
    return False


def is_patch_checkable(patch):
    content = ''
    with open(patch, 'r') as fp:
        content = fp.read()
    for line in content.split('\n'):
        line = line.strip()
        # stable backport
        if re.match("commit [0-9a-f]{8,40} upstream", line, re.IGNORECASE):
            return False
        # stable backport
        if re.match("\[\s*upstream commit [0-9a-f]{8,40}\s*\]", line, re.IGNORECASE):
            return False
        if line in ["Conflicts:"]:
            return True
    return False


def is_patch_revert(patch):
    content = ''
    with open(patch, 'r') as fp:
        content = fp.read()
    for line in content.split('\n'):
        line = line.strip()
        if "This reverts commit" in line:
            return True
    return False


def do_checkpatch(patch):
    commit = patch.split(" ")[0]

    cmd = "git format-patch -1 " + commit + " -o checkpatch"
    ret, out = execute_shell_unicode(cmd)
    if ret:
        return 1, out

    path = out.strip()

    ret = is_from_mainline(path)
    if ret:
        if not is_patch_checkable(path):
            description = " "
            description += 'total: 0 errors, 0 warnings, 0 lines checked'
            return 0, description
    else:
        if is_patch_revert(path):
            description = "patch is reverted\n"
            description += 'total: 0 errors, 0 warnings, 0 lines checked'
            return 0, description

    cmd = "./scripts/checkpatch.pl " + out.strip() + " --show-types"
    if len(ignores_for_main):
        cmd = cmd + " --ignore "
        for ignore in ignores_for_main:
            cmd = cmd + ignore + ','
    ret, out = execute_shell_unicode(cmd)

    _regex = 'total: .* checked'
    if re.search(_regex, out):
        description = re.search(_regex, out).group(0)
        if description.find(' 0 error') == -1:
            return 1, out
        elif description.find(' 0 warning') == -1:
            return 2, out
        else:
            return 0, out
    else:
        return 0, 'total: 0 errors, 0 warnings, 0 lines checked'


def main(argv):
    if len(sys.argv) != 1:
        opts, args = getopt.getopt(argv[1:], 's:')
        patches = sys.argv[1:]
        for o, v in opts:
            if o == '-s':
                patches = os.popen("git log --oneline " + v + " -1").readlines()
                patches = patches + os.popen("git log --oneline " + v + "... | tac").readlines()
    else:
        tag = sys.argv[1]
        patches = os.popen("git log --oneline " + tag + "..HEAD").readlines()

    failed_num = 0
    warning_num = 0
    for patch in patches:
        patch = patch.strip()
        print("check " + patch)

        ret, description = do_checkpatch(patch)
        if ret == 1:
            print("check " + patch + " failed")
            print(description)
            failed_num = failed_num + 1
        elif ret == 2:
            print("check " + patch + " warning")
            print(description)
            warning_num += 1
        else:
            print("check " + patch + " success")
            print(description)

    print("---- result ----")
    print("total:" + str(len(patches)) + " failed:" + str(failed_num) + " warning:" + str(warning_num)
          + " success:" + str(len(patches) - failed_num - warning_num))

    clean()
    return True


if __name__ == "__main__":
    # execute only if run as a script
    print('begin to checkpatch....')
    main(sys.argv)
