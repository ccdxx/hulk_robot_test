#!/usr/bin/python
# -:w*- coding: UTF-8 -*-

import configparser
import os
import re
import sys
import subprocess
import shlex
import getopt

dir = os.path.split(os.path.realpath(__file__))[0]

linux_path = ''
source_branch = ''
target_branch = ''

def config_parser():
    global linux_path
    global source_branch
    global target_branch

    parser = configparser.RawConfigParser()
    parser.optionxform = lambda option: option
    parser.read(dir + '/../config')
    linux_path = parser.get('checkdepend', 'linux_path')
    source_branch = parser.get('checkdepend', 'source_branch')
    target_branch = parser.get('checkdepend', 'target_branch')

config_parser()
