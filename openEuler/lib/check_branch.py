import argparse
import os
import sys

import yaml


def init_args():
    """
    init args
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", type=str, dest="config", help="config file")
    parser.add_argument("-b", type=str, dest="branch", help="target branch")
    parser.add_argument("-a", type=str, dest="arch", help="build arch")

    return parser.parse_args()


if __name__ == '__main__':
    args = init_args()
    if not args.branch or not args.arch or not args.config:
        sys.exit(2)

    branch = args.branch
    arch = args.arch
    config = args.config
    current_path = os.path.dirname(os.path.abspath(__file__))
    yaml_path = os.path.join(os.path.dirname(current_path), 'conf', config)
    if not os.path.exists(yaml_path):
        print(f'{config} not found in conf path, exit')
        sys.exit(2)

    with open(yaml_path, 'r', encoding='utf-8') as f:
        result = yaml.safe_load(f)

    if branch not in result:
        print(f'{branch} not in yaml, exit')
        sys.exit(2)

    archs = result.get(branch)
    if arch not in archs:
        print(f'{arch} not in yaml, exit')
        sys.exit(2)

    if not archs.get(arch):
        print(f'{arch} is set to false, exit')
        sys.exit(1)
