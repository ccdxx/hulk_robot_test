#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import re
import stat
import sys
import logging.config
import json
import argparse
import warnings
import yaml
import operator

from yaml.error import YAMLError
from src.ac.framework.ac_result import ACResult, SUCCESS
from src.proxy.gitee_proxy import GiteeProxy
from src.proxy.kafka_proxy import KafkaProducerProxy
from src.proxy.jenkins_proxy import JenkinsProxy
from src.utils.dist_dataset import DistDataset


class Comment(object):
    """
    comments process
    """

    def __init__(self, pr, jenkins_proxy, *check_item_comment_files):
        """

        :param pr: pull request number
        """
        self._pr = pr
        self._check_item_comment_files = check_item_comment_files
        self._up_builds = []
        self._up_up_builds = []
        self._get_upstream_builds(jenkins_proxy)
        self.ac_result = {}
        self.compare_package_result = {}
        self.check_item_result = {}

    def comment_build(self, gitee_proxy):
        """
        构建结果
        :param jenkins_proxy:
        :param gitee_proxy:
        :return:
        """
        comments = self._comment_build_html_format()
        gitee_proxy.comment_pr(self._pr, "\n".join(comments))

        return "\n".join(comments)


def init_args():
    """
    init args
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", type=int, dest="pr", help="pull request number")
    parser.add_argument("-m", type=str, dest="comment_id", help="uniq comment id")
    parser.add_argument("-c", type=str, dest="committer", help="commiter")
    parser.add_argument("-o", type=str, dest="owner", help="gitee owner")
    parser.add_argument("-r", type=str, dest="repo", help="repo name")
    parser.add_argument("-t", type=str, dest="gitee_token", help="gitee api token")

    parser.add_argument("-b", type=str, dest="jenkins_base_url", default="https://openeulerjenkins.osinfra.cn/",
                        help="jenkins base url")
    parser.add_argument("-u", type=str, dest="jenkins_user", help="repo name")
    parser.add_argument("-j", type=str, dest="jenkins_api_token", help="jenkins api token")
    parser.add_argument("-f", type=str, dest="check_result_file", default="", help="compare package check item result")
    parser.add_argument("-a", type=str, dest="check_item_comment_files", nargs="*", help="check item comment files")

    parser.add_argument("--disable", dest="enable", default=True, action="store_false", help="comment to gitee switch")
    parser.add_argument("-kpath", type=str, dest="kernel_path", help="kernel path")

    parser.add_argument("-A", type=str, dest="arch", help="arch")
    parser.add_argument("-C", type=str, dest="committer_dict", help="committer dict for report")

    return parser.parse_args()


def _build_result(kernel_path, arch):
    """
    Get chekbuild result
    """
    comment_str = f'{arch} check_build WARNING: Check Details at [#{str(trigger_build_id)}]({url_check})\n'
    commits = []
    with open(f'{kernel_path}/build_output.txt', 'r') as fp:
        for line in fp:
            commits.append(line)
    commits_str = '\n'.join(commits)
    commits_str = f"""
<details>
  <summary>Click for more info</summary>
  <pre><code>
{commits_str}
  </code></pre>
</details>
    """
    comment_str = comment_str + commits_str
    return comment_str


def _custom_result(kernel_path):
    """
    Get static check result
    """
    failed = False
    commits = []
    commits.append("<table>\
    <tr>\
    <th width=40%>Check Name</th>\
    <th width=30%>Check Result</th>\
    <th width=30%>Check Details</th>\
    </tr>")

    pflag = False
    pwarning = False
    with open(os.path.join(kernel_path, 'checkpatchres'), 'r') as fp:
        data = fp.read()
        pcomment_str = ''
        if data.find('failed:0') == -1:
            pflag = True
            pcomment_str = f"checkpatch FAILED: Check Details at [#{str(trigger_build_id)}]({url_check}/console)\n"
        elif data.find('warning:0') == -1:
            pwarning = True
            pcomment_str = f"checkpatch WARNING: Check Details at [#{str(trigger_build_id)}]({url_check}/console)\n"
        pcommits_str = f"""
<details>
  <summary>Click for more info\n</summary>
  <pre><code>
{data}
  </code></pre>
</details>
    """
        pcomment_str = pcomment_str + pcommits_str

    fflag = False
    with open(os.path.join(kernel_path, 'checkformatres'), 'r') as fp:
        data = fp.read()
        if 'failed:' in data:
            fflag = True
            fcomment_str = f"checkformat FAILED: Check Details at [#{str(trigger_build_id)}]({url_check}/console)\n"
            # commits_str = '\n'.join(data)
            fcommits_str = f"""
<details>
  <summary>Click for more info\n</summary>
  <pre><code>
{data}
  </code></pre>
</details>
    """
            fcomment_str = fcomment_str + fcommits_str

    dflag = False
    with open(os.path.join(kernel_path, 'checkdependres'), 'r') as fp:
        data = fp.read()
        if 'check failed' in data:
            dflag = True
            dcomment_str = f"checkdepend FAILED: Check Details at [#{str(trigger_build_id)}]({url_check}/console)\n"
            # commits_str = '\n'.join(data)
            dcommits_str = f"""
<details>
  <summary>Click for more info\n</summary>
  <pre><code>
{data}
  </code></pre>
</details>
    """
            dcomment_str = dcomment_str + dcommits_str

    if pflag:
        pres = ":x: FAILED"
    elif pwarning:
        pres = ":bug: WARNING"
    else:
        pres = ":white_check_mark: SUCCESS"
    commits.append("<tr>\
        <th>checkpatch</th>\
        <th>{}</th>\
        <th rowspan=3><a href={}>{}{}</a></th>\
        </tr>".format(pres, "{}{}".format(url_check, "/console"), "#", trigger_build_id))
    commits.append("<tr>\
        <th>checkformat</th>\
        <th>{}</th>\
        </tr>".format(":x: FAILED" if fflag else ":white_check_mark: SUCCESS"))
    commits.append("<tr>\
        <th>checkdepend</th>\
        <th>{}</th>\
        </tr>\
        </table>".format(":x: FAILED" if dflag else ":white_check_mark: SUCCESS"))

    comment_str = '\n'.join(commits)
    if pflag or pwarning:
        comment_str += '\n\n' + pcomment_str

    if fflag:
        comment_str += '\n\n' + fcomment_str

    if dflag:
        comment_str += '\n\n' + dcomment_str

    if any([fflag, dflag]):
        failed = True
    return failed, comment_str


def _at(user):
    return "@{} ".format(user)

def _link(user):
    return "<a href=https://gitee.com/{}>{}</a>".format(user, user)

def _to_sequence(owner):
    committer_seq = ""
    recommend = owner.pop()
    for i in recommend:
        committer_seq = committer_seq + _at(i) + ", "
    last = recommend
    while len(owner) != 0:
        candidate = owner.pop()
        if operator.eq(candidate, last):
            continue
        else:
            last = candidate
        committer_seq = committer_seq + ":fa-sign-out: "
        for i in candidate:
            committer_seq = committer_seq + _link(i) + ", "
    return committer_seq

def _committer_result(kernel_path, committer_dict):
    """
    Get committer result
    """
    commits = []
    commits.append("<table>\
    <tr>\
    <th width=40%>Modified Files</th>\
    <th width=60%>Committer Sequence</th>\
    </tr>")

    with open(f'{kernel_path}/{committer_dict}', 'r') as f:
        ctx = f.read()
    committer_info = json.loads(ctx)

    for file in list(committer_info.keys()):
        owner = committer_info.pop(file, None)
        if owner is None:
            continue
        count = 1
        buddy = []
        for key in list(committer_info.keys()):
            if operator.eq(owner, committer_info[key]):
                committer_info.pop(key)
                count = count + 1
                buddy.append(key)

        commits.append("<tr>\
            <th>{}</th>\
            <th rowspan={}>{}</th>\
            </tr>".format(file, count, _to_sequence(owner)))

        for i in buddy:
            commits.append("<tr>\
                <th>{}</th>\
                </tr>".format(i))

    commits.append("</table>")
    comment_str = '\n'.join(commits)
    comment_str += "\n\n**以上为各修改文件匹配的推荐Committer序列，需各模块评审通过后方可合入。**"
    return comment_str


if "__main__" == __name__:
    args = init_args()
    if not args.enable:
        sys.exit(0)

    gp = GiteeProxy(args.owner, args.repo, args.gitee_token)
    trigger_job_name = os.environ.get("JOB_NAME")
    trigger_build_id = os.environ.get("BUILD_ID")
    debug_info = os.environ.get("DEBUG_INFO")
    jp = JenkinsProxy(args.jenkins_base_url, args.jenkins_user, args.jenkins_api_token)
    url, build_time, reason = jp.get_job_build_info(trigger_job_name, int(trigger_build_id))

    url_check = url + str(trigger_build_id)

    build_arch = re.search(r'multiarch/job/openeuler/job/(.*)/job', url).group(1)

    if args.kernel_path:
        if args.arch:
            comment_str = _build_result(args.kernel_path, args.arch)
        elif args.committer_dict:
            comment_str = _committer_result(args.kernel_path, args.committer_dict)
        else:
            _, comment_str = _custom_result(args.kernel_path)
    else:
        if debug_info is None:
            sys.exit(0)

        comments = ["门禁正在运行， 您可以通过以下链接查看实时门禁检查结果."]

        trigger_job_info = jp.get_job_info(trigger_job_name)
        trigger_job_url = trigger_job_info.get("url")
        comments.append(f"门禁入口及编码规范检查: <a href={trigger_job_url}>{jp.get_job_path_from_job_url(trigger_job_url)}</a>, 当前构建号为 <a href={url_check}/console>{trigger_build_id}</a>")

        down_projects = trigger_job_info.get("downstreamProjects", [])
        build_job_name_list = []
        build_job_link_list = []
        for project in down_projects:
            build_job_url = project.get("url", "")
            if build_job_url:
                build_job_name = jp.get_job_path_from_job_url(build_job_url)
                build_job_name_list.append(build_job_name)
                build_job_link_list.append("<a href={}>{}</a>".format(build_job_url, build_job_name))
        comments.append("构建及构建后检查: {}".format(", ".join(build_job_link_list)))

        if build_job_name_list:
            build_job_info = jp.get_job_info(build_job_name_list[0])
            down_down_projects = build_job_info.get("downstreamProjects", [])
            for project in down_down_projects:
                comment_job_url = project.get("url")
                comments.append("门禁结果回显: <a href={}>{}</a>".format(
                    comment_job_url, jp.get_job_path_from_job_url(comment_job_url)))
        comments.append("若您对门禁结果含义不清晰或者遇到问题不知如何解决，可参考"
                         "<a href=https://www.openeuler.org/zh/blog/zhengyaohui/2022-03-21-ci_guild.html>门禁指导手册</a>")
        comment_str = "\n".join(comments)
    # ret=gp.comment_pr(args.pr, "\n".join(commits))
    ret = gp.comment_pr(args.pr, comment_str)
    if ret:
        print("comment pr success")
    else:
        print("comment pr faild")
