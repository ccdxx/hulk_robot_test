#!/bin/bash

current_path=$(pwd)
shell_path=$current_path/openeuler-jenkins
. $test_path/lib/common.sh

function download_linux_kernel() {
    log_info "***** Start to download kernel of Linux *****"
    cd $current_path
    rm -rf $current_path/Linux
    for i in $(seq 300);do
        git clone --depth 1 https://gitee.com/mirrors/Linux 1> /dev/null
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    if [[ $exitcode -ne 0 ]];then
        log_error "git clone Linux kernel failed"
    fi
    log_info "***** End to download kernel of Linux *****"
}

function setup_env() {
    log_info "***** Setup environment variable for check scripts *****"
    cd $current_path/Linux
    kernel_tag=$tbranch
    linux_main_dir=$(pwd)
    echo "linux_path = $linux_main_dir" >> $test_path/lib/static_checking/config
    echo "target_branch = $kernel_tag" >> $test_path/lib/static_checking/config
    echo "source_branch = $kernel_tag" >> $test_path/lib/static_checking/config
}

function run_static_check() {
    log_info "***** Run python check scripts *****"
    cd $current_path/openeuler/nestos-kernel
    python3 $test_path/lib/static_checking/scripts/pr_checkpatch.py -s HEAD~$backwards_count > $current_path/openeuler/nestos-kernel/checkpatchres
    python3 $test_path/lib/static_checking/scripts/format.py -s HEAD~$backwards_count > $current_path/openeuler/nestos-kernel/checkformatres
    python3 $test_path/lib/static_checking/scripts/depend.py -s HEAD~$backwards_count > $current_path/openeuler/nestos-kernel/checkdependres
}

function fetch_checkcustom_result() {
    cd $current_path/openeuler/nestos-kernel
    log_info "$(<checkpatchres)"
    grep "failed:0" checkpatchres >/dev/null
    if [[ $? -ne 0 ]];then
        log_warn "checkpatch result: failed"
        log_info "$(<checkpatchres)"
    else
        grep "warning:0" checkpatchres >/dev/null
        if [[ $? -ne 0 ]];then
            log_warn "checkpatch result: warning"
            log_info "$(<checkpatchres)"
        else
            log_info "checkpatch result: successful"
        fi
    fi

    grep "failed:" checkformatres >/dev/null
    if [[ $? -eq 0 ]];then
        log_warn "checkformat result: failed"
        log_info "$(<checkformatres)"
    else
        log_info "checkformat result: successful"
    fi

    grep "check failed" checkdependres >/dev/null
    if [[ $? -eq 0 ]];then
        log_warn "checkdepend result: failed"
        log_info "$(<checkdependres)"
    else
        log_info "checkdepend result: successful"
    fi
}

function report_committer() {
    cd $current_path/openeuler/kernel
    python3 $test_path/lib/static_checking/scripts/get_committer.py -s HEAD~$backwards_count -i $test_path/conf/committers.json -o committer_res.json
    if [[ $? -ne 0 ]];then
        log_warn "failed to report committer"
	return
    fi
    python3 $test_path/lib/pr_comment_api.py -o $owner -r $giteeRepoName -p $prid -c $giteeCommitter -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentID} -kpath ${current_path}/openeuler/kernel -C committer_res.json
}

function main() {
    import_openeuler_jenkins
    # python3 $test_path/lib/pr_comment_api.py -r $giteeRepoName -o $owner -p $giteePullRequestIid -c $giteeCommitter -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentID}
    git_config
    download_linux_kernel
    # tbranch=$giteeTargetBranch
    download_openeuler_kernel
    # prid=$giteePullRequestIid
    git_am_pr
    patches_count=$(git log --oneline origin/$tbranch..HEAD | wc -l)
    backwards_count=$((patches_count-1))
    setup_env
    # report_committer
    run_static_check
    log_info "***** Print checking results *****"
    cd  $current_path/openeuler/nestos-kernel
    fetch_checkcustom_result
    python3 $test_path/lib/pr_comment_api.py -o $owner -r $repo -p $prid -c $committer -t $GiteeToken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -kpath ${current_path}/openeuler/nestos-kernel
}

main "$@"
