#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0
#
# Copyright (C) 2023 Huawei Technologies Co., Ltd.
# Authors: Wei Li <liwei391@huawei.com>

'''
The maintenance tool for committers.json

Usage: %s [option [args]...]
Options:
  -i <file>     Specify committers file, "committers.json" as default if not set
  -p <path>     Target file or dir to process
  -g            Get all committers of 'path'
  -l <name>     List all maintained files of a committer
  -a <name>     Add a committer for 'path'
  -d <name>     Del a committer for 'path'
  -h, --help    Show this help info
'''

import getopt
import sys
import json


COM_FILE = 'committers.json'
committer_conf = {}


def get_committer_files(node, owner, prefix=""):
    files = []
    if owner in node["owner"]:
        files.append(prefix)

    if len(node.get("children", {})) == 0:
        return files

    for ch in list(node["children"].keys()):
        files += get_committer_files(node["children"][ch], owner, prefix + "/" + ch)

    return files


def add_file_committer(path, owner):
    paths = path.split("/")
    global committer_conf
    curr = committer_conf["/"]["children"]
    i = 0
    while i < len(paths):
        if paths[i] not in curr:
            curr.setdefault(paths[i], {"owner": [], "children": {}})
        if i == len(paths) - 1:
            if owner not in curr[paths[i]]["owner"]:
                if "UNMAINTAINED" in curr[paths[i]]["owner"]:
                    curr[paths[i]]["owner"].remove("UNMAINTAINED")
                curr[paths[i]]["owner"].append(owner)
            return curr[paths[i]]["owner"]
        curr = curr[paths[i]]["children"]
        i += 1
    return None


def del_file_committer(path, owner):
    paths = path.split("/")
    global committer_conf
    curr = committer_conf["/"]["children"]
    i = 0
    while i < len(paths):
        if paths[i] not in curr:
            break
        if i == len(paths) - 1:
            if owner in curr[paths[i]]["owner"]:
                curr[paths[i]]["owner"].remove(owner)
            return curr[paths[i]]["owner"]
        curr = curr[paths[i]]["children"]
        i += 1
    return None


def lookup_committer(path):
    committers = []
    committers.append(committer_conf["/"]["owner"])
    curr = committer_conf["/"]["children"]

    paths = path.split("/")
    for path in paths:
        if path not in curr:
            break
        else:
            owner = curr[path].get("owner", [])
            if len(owner) == 0:
                pass
            else:
                committers.append(owner)
            curr = curr[path].get("children", {})

    committers.reverse()
    return committers


def do_process(option, path, owner):
    ret = 0
    update = False
    if option == 'LIST':
        files = get_committer_files(committer_conf["/"], owner)
        print("Maintained files of {}:".format(owner))
        print(files)
    elif option == 'GET':
        committers = lookup_committer(path)
        print("Committer sequence for {}:".format(path))
        print(committers)
    elif option == 'ADD':
        committers = add_file_committer(path, owner)
        print("Add {} to committer list of file {}: Success".format(owner, path))
        print("Committers for {}: {}".format(path, committers))
        update = True
    elif option == 'DEL':
        committers = del_file_committer(path, owner)
        print("Del {} to committer list of file {}: Success".format(owner, path))
        print("Committers for {}: {}".format(path, committers))
        update = True
    return ret, update


if __name__ == "__main__":
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'i:p:l:a:d:gh', ['help'])
        for opt, val in opts:
            if opt in ('-h', '--help'):
                print(__doc__ % (sys.argv[0]))
                sys.exit(0)
            elif opt in ('-i'):
                COM_FILE = val
                continue
            elif opt in ('-p'):
                F_PATH = val.strip("/")
                continue
            elif opt in ('-g'):
                PROCESS = 'GET'
                OWNER = None
                continue
            elif opt in ('-l'):
                PROCESS = 'LIST'
                OWNER = val
                F_PATH = None
                continue
            elif opt in ('-a'):
                PROCESS = 'ADD'
                OWNER = val
                continue
            elif opt in ('-d'):
                PROCESS = 'DEL'
                OWNER = val
                continue
        if 'PROCESS' not in globals():
            raise getopt.GetoptError("Error: Lack of process type.")
    except getopt.GetoptError as err:
        print(err)
        print(__doc__ % (sys.argv[0]))
        sys.exit(-1)

    with open(COM_FILE, 'r') as f:
        ctx = f.read()
    committer_conf = json.loads(ctx)

    ret, update = do_process(PROCESS, F_PATH, OWNER)
    if update == True:
        with open('committers.json', 'w') as f:
            json.dump(committer_conf, f, indent=4)

    sys.exit(ret)
