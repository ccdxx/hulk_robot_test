### 文件作用说明

- `checkbuild.sh` 交叉编译检查(arm,ppc,ppc86,riscv64)调用的脚本
    build_steps执行步骤：
    ``` shell
    ARCH="powerpc"
    CROSS_PREFIX="powerpc64"
    rm -rf hulk_robot_test/
    for i in $(seq 300);do
        git clone https://gitee.com/zhixiuzhou/hulk_robot_test.git
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    export test_path=$(pwd)/hulk_robot_test/openEuler
    bash hulk_robot_test/openEuler/checkbuild.sh $ARCH $CROSS_PREFIX
  ```
- `checkcustom.sh` 静态检查调用的检查脚本
    build_steps执行步骤：
    ``` shell
    rm -rf hulk_robot_test/
    for i in $(seq 300);do
        git clone https://gitee.com/zhixiuzhou/hulk_robot_test.git
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    export test_path=$(pwd)/hulk_robot_test/openEuler
    bash hulk_robot_test/openEuler/checkcustom.sh
  ```
- `checkkabi.sh` 编译x86-64和aarch64并检查kabi的检查脚本
    build_steps执行步骤：
    ``` shell
    rm -rf hulk_robot_test/
    for i in $(seq 300);do
        git clone https://gitee.com/zhixiuzhou/hulk_robot_test.git
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    export test_path=$(pwd)/hulk_robot_test/openEuler
    bash hulk_robot_test/openEuler/checkkabi.sh $arch
  ```


- `conf` 配置文件归档目录
- `lib` 包含静态检查的执行脚本和发送评论的python脚本
- `tools` 包含交叉编译的gcc

### 路径说明(以aarch64为例)
- 环境默认处于的路径:  /home/jenkins/agent/workspace/multiarch/openeuler/aarch64/kernel
- current_path=环境默认处于的路径
- test_path=$current_path/hulk_robot_test/openEuler
- shell_path=$current_path/openeuler-jenkins
- kernel代码路径: $current_path/openeuler/kernel
- kabi白名单路径: $current_path/src-openeuler/kernel
