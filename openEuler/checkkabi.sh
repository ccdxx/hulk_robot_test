#!/bin/bash

current_path=$(pwd)
shell_path=$current_path/openeuler-jenkins
NR_CPUS=$(nproc)
KABI_CHECK=1
IS_SKIP=0
. $test_path/lib/common.sh

function get_check_kabi_script() {
    log_info "***** Get the corresponding check-kabi script *****"
    cd $current_path
    git init src-openeuler
    export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
    cd src-openeuler
    for i in $(seq 300);do
        git clone https://gitee.com/src-openeuler/nestos-kernel.git 1> /dev/null
        exitcode=$?
        if [[ $exitcode -eq 0 ]];then
            break
        fi
        sleep 5
    done
    if [[ $exitcode -ne 0 ]];then
        log_error "git clone src-openeuler nestos-kernel failed"
    fi
    cd nestos-kernel
    if [[ $tbranch == "master" ]];then
        git checkout openEuler-22.03-LTS-SP2 > /dev/null
    elif [[ $tbranch == "hostos-22.03-dev" ]];then
        git checkout openEuler-22.03-LTS-SP2 > /dev/null
    elif [[ $tbranch == "nestos-22.03-LTS-SP3" ]];then
        git checkout Multi-Version_NestOS-For-Container_openEuler-22.03-LTS-SP3 > /dev/null
    elif [[ $tbranch == "NLK-6.6" ]];then
        git checkout Multi-Version_NestOS-For-Container_openEuler-24.03-LTS-SP1 > /dev/null
    else
        git checkout $tbranch > /dev/null
    fi
    if [[ $? -ne 0 ]];then
        KABI_CHECK=0
    fi
    chmod +x check-kabi
    if [[ $? -ne 0 ]];then
        KABI_CHECK=0
    fi
}

function build_allmodconfig() {
    log_info "***** Build kernel with allmodconfig *****"
    cd $current_path/openeuler/nestos-kernel
    echo "| check | result |" > $current_path/openeuler/nestos-kernel/result
    make clean
    make allmodconfig
    make oldconfig
    make -j$NR_CPUS 1> /dev/null

    if [[ $? -eq 0 ]];then
        log_info "$arch allmodconfig build pass"
        echo "| $arch allmodconfig build | pass |">> $current_path/openeuler/nestos-kernel/result
    else
        log_warn "$arch allmodconfig build fail"
        echo "| $arch allmodconfig build | fail |">> $current_path/openeuler/nestos-kernel/result
    fi
}

function build_defconfig() {
    log_info "***** Build kernel with openeuler_defconfig *****"
    cd $current_path/openeuler/nestos-kernel
    make openeuler_defconfig
    make -j$NR_CPUS 1> /dev/null

    if [[ $? -eq 0 ]];then
        log_info "$arch build pass"
        echo "| $arch build | pass |">> $current_path/openeuler/nestos-kernel/result
    else
        log_warn "$arch build fail"
        echo "| $arch build | fail |">> $current_path/openeuler/nestos-kernel/result
    fi
}

function check_kabi() {
    log_info "***** Check kabi compatibility *****"
    if [[ $KABI_CHECK -ne 1 ]];then
        log_info "For $tbranch branch, kabi compatibility is NOT mandatory, skip kabi checking "
        exit 0
    fi
    cd $current_path/openeuler/nestos-kernel
    if [[ -e $current_path/src-openeuler/nestos-kernel/Module.kabi_${arch} ]];then
        $current_path/src-openeuler/nestos-kernel/check-kabi -k $current_path/src-openeuler/nestos-kernel/Module.kabi_${arch} -s ./Module.symvers
        if [[ $? -eq 0 ]];then
            log_info "$arch checkkabi pass"
            echo "| $arch checkkabi | pass |">> result
        else
            log_warn "$arch checkkabi fail"
            echo "| $arch checkkabi | fail |">> result
        fi
    else
        log_info "For $tbranch branch, kabi compatibility is NOT mandatory, skip kabi checking "
    fi
}

function main() {
    python3 $test_path/lib/check_branch.py -b $tbranch -a $arch -c check_build.yaml
    IS_SKIP=$?
    if [[ $IS_SKIP -ne 0 ]];then
        exit 0
    fi
    echo "***** Remove old directories *****"
    rm -rf $current_path/src-openeuler
    rm -rf $current_path/openeuler
    import_openeuler_jenkins
    # python3 $test_path/lib/pr_comment_api.py -o $owner -r $repo -p $prid -c $committer -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentid}
    git_config
    get_check_kabi_script
    download_openeuler_kernel
    install_build_tools
    git_am_pr
    build_allmodconfig
    build_defconfig
    # check_kabi
    # if [[ -s $current_path/openeuler/nestos-kernel/build_output.txt ]];then
    #     log_info "build warning: $(<$current_path/openeuler/nestos-kernel/build_output.txt)"
    #     python3 $test_path/lib/pr_comment_api.py -o $owner -r $repo -p $prid -c $committer -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentid} -A $arch -kpath ${current_path}/openeuler/nestos-kernel
    # fi
    grep "fail" $current_path/openeuler/nestos-kernel/result >/dev/null
    if [[ $? -eq 0 ]];then
        exit 1
    else
        exit 0
    fi
}

main "$@"
