#!/bin/bash

current_path=$(pwd)
shell_path=$current_path/openeuler-jenkins
. $test_path/lib/common.sh
NR_CPUS=$(nproc)
IS_SKIP=0

function setup_gcc() {
    log_info "***** Setup gcc *****"
    sudo mkdir -p /usr/local/$ARCH
    sudo chmod 777 /usr/local/$ARCH
    cd /usr/local/$ARCH

    if [[ $ARCH == "arm" ]];then
        CROSS_COMPILE="arm-linux-gnueabi-"
        tar -zxf $test_path/tools/x86_64-gcc-12.2.0-nolibc-arm-linux-gnueabi.tar.gz -C $test_path/tools/ 1>/dev/null
        cp -r $test_path/tools/gcc-12.2.0-nolibc/$ARCH-linux-gnueabi /usr/local/$ARCH
        export PATH=$PATH:/usr/local/$ARCH/$ARCH-linux-gnueabi/bin
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/$ARCH/$ARCH-linux-gnueabi/lib
    else
        CROSS_COMPILE="$CROSS_PREFIX-linux-"
        tar -zxf $test_path/tools/x86_64-gcc-12.2.0-nolibc-$CROSS_PREFIX-linux.tar.gz -C $test_path/tools/ 1>/dev/null
        cp -r $test_path/tools/gcc-12.2.0-nolibc/$CROSS_PREFIX-linux /usr/local/$ARCH
        export PATH=$PATH:/usr/local/$ARCH/$CROSS_PREFIX-linux/bin
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/$ARCH/$CROSS_PREFIX-linux/lib
    fi
}

function build_kernel() {
    make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE allmodconfig
    make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE -j$NR_CPUS 1>/dev/null
    if [[ $? -ne 0 ]]; then
        log_error "build failed"
    fi
    git_am_pr
    make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE allmodconfig
    make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE -j$NR_CPUS 1>/dev/null 2> build_output.txt
}

function main() {
    if [[ $# -ne 2 ]];then
        echo "need arch and cross compile prefix"
    fi
    ARCH=$1
    CROSS_PREFIX=$2
    python3 $test_path/lib/check_branch.py -b $tbranch -a $arch -c check_build.yaml
    IS_SKIP=$?
    if [[ $IS_SKIP -ne 0 ]];then
        exit 0
    fi
    import_openeuler_jenkins
    python3 $test_path/lib/pr_comment_api.py -o $owner -r $repo -p $prid -c $committer -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentid}
    git_config
    setup_gcc
    install_build_tools
    download_openeuler_kernel
    build_kernel
    exitcode=$?
    if [[ -s $current_path/openeuler/kernel/build_output.txt ]];then
        log_info "build warning: $(<$current_path/openeuler/kernel/build_output.txt)"
        python3 $test_path/lib/pr_comment_api.py -o $owner -r $repo -p $prid -c $committer -t $giteetoken -b $jenkins_api_host -u $jenkins_user -j $jenkins_api_token -m ${commentid} -A $ARCH -kpath ${current_path}/openeuler/kernel
    fi
    exit $exitcode
}

main "$@"